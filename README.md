# Kubernetes Agent Example

## About

This is a [configuration repository example project](https://docs.gitlab.com/ee/user/clusters/agent/#defining-a-configuration-repository) of the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/).

## Setup

To use this project correctly, please follow the entire setup example in the [GitLab Kubernetes Agent documentation](https://docs.gitlab.com/ee/user/clusters/agent/) .
